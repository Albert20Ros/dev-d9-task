<?php
namespace Drupal\Entity_rest_db\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\user\UserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Url;

/**
 * Provides a resource to get user by email.
 *
 * @RestResource(
 *   id = "db_entity",
 *   label = @Translation("DB entity"),
 *   uri_paths = {
 *     "canonical" = "/db_entity"
 *   }
 * )
 */
class Entity_rest_db extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('dummy'),
      $container->get('current_user')
    );
  }
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the  entity.'))
      ->setReadOnly(TRUE)
      ->setSettings([
        'max_length' => 12
      ]);


    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the  entity.'))
      ->setReadOnly(TRUE);


    $fields['ticket_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ticket_id'))
      ->setDescription(t('field ticket_id'))
      ->setSettings([
        'max_length' => 12,
        'default_value' => 0,
      ]);

    $fields['user_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('user_id'))
      ->setDescription(t(' field user_id'))
      ->setSettings([
        'max_length' => 12,
        'default_value' => 0,
      ]);

    // String field.
    $fields['bin_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('bin_id'))
      ->setDescription(t('field bin_id'))
      ->setSettings([
        'max_length' => 12,
        'default_value' => 0,
      ]);

    $fields['created'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('created'))
      ->setDescription(t('field created'))
      ->setSettings([
        'max_length' => 12,
        'default_value' => '',
      ]);
    $fields['action'] = BaseFieldDefinition::create('varchar')
      ->setLabel(t('action'))
      ->setDescription(t('field action'))
      ->setSettings([
        'max_length' => 25,
        'collate' => 'utf8_unicode_ci',
        'default_value' => ''
      ]);

    $fields['hours'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('hours'))
      ->setDescription(t('field hours'))
      ->setSettings([
        'precision' => 10,
        'scale' => 2,
        'default_value' => ''
      ]);
    $fields['entry'] = BaseFieldDefinition::create('string')
      ->setLabel(t('entry'))
      ->setDescription(t('field entry'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 250,
        'text_processing' => 0,
      ]);

    return $fields;
  }
  public function get(){
     return new ResourceResponse('Label: ID
         description: The ID of the  entity.', 200);


  }
}
