<?php
/**
 * @return
 * Contains \Drupal\helloworld\Controller\FirstPageController.
 */
namespace Drupal\helloworld\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides route responses for the DrupalBook module.
 */
class FirstPageController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function content() {
    return [
      '#markup' => $this->t('Hello, World!'),
    ];
  }
  public function dossier() {
    return [
      '#markup' => $this->t('This is dossier page!'),
    ];
  }


  public function rainbow() {
    return [
      '#markup' => 'Привет, пользователь! серверное время:' . gmdate("H:i:s", REQUEST_TIME),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
