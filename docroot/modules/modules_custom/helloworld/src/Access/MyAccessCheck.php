<?php

namespace Drupal\helloworld\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Routing;
use Drupal\Core\Routing\RouteMatchInterface;
/**
 * My custom access check.
 */
class MyAccessCheck implements AccessInterface {

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account)
  {
    //return AccessResult::allowedIf(($account->id() == 1)|($account->id() == \Drupal::routeMatch()->getParameter('uid')));
    if ($account->id() == 1) {
      return AccessResult::allowed();
    } elseif (\Drupal::RouteMatch()->getRawParameter('uid') == $account->id()) {
      return AccessResult::allowed();}
    elseif ($account->id() == 0){
      return AccessResult::forbiden();
    } else {
      return AccessResult::neutral();
    }

  }
}
