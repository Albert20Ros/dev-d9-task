<?php
namespace Drupal\produce_new_essence\essence;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the dossier entity
 *
 * @ingroup dossier
 *
 * * @ContentEntityType(
 *   id = "dossier",
 *   label = @Translation("dossier"),
 *   base_table = "dossier",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     path: '/dossier'
 *   },
 * )
 */
class essence extends ContentEntityBase implements ContentEntityInterface{

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Dossier essence.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Dossier essence.'))
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      -> setLabel(t('Name'))
      -> setDescription('Name of essence')
      -> setRequired('true');

    $fields['position'] = BaseFieldDefinition::create('string')
      -> setLabel(t('Position'))
      -> setDescription('Position of essence');

    $fields['biography'] = BaseFieldDefinition::create('string')
      -> setLabel(t('Biography'))
      -> setDescription('Bio of essence');

    return $fields;

  }



}
