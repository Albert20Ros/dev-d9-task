<?php
/**
 * @file
 * Contains \Drupal\b24_form_block\Form.
 */
namespace Drupal\b24_form_block\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class BitrixForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'BitrixForm';
  }
  /**
   * {@inheritdoc}
   * Form
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    # Text field
    $form['your_name'] = array(
      '#type' => 'textfield',
      '#attributes' => array(
        'placeholder' => 'Ваше Имя',
      ),
    );
    $form['E-mail']=array(
      '#type' => 'textfield',
      '#attributes' => array(
        'placeholder' => 'Ваш E-mail',
      ),

    );
    $form['phone']=array(
      '#type' => 'textfield',
      '#attributes' => array(
        'placeholder' => 'Ваш Телефон',
      ),

    );
    $form['comment']=array(
      '#type' => 'textarea',
      '#attributes' => array(
        'placeholder' => 'Ваш Комментарий',
      ),

    );
    $form['personal-date']=array(
      '#title' =>'Заполняя форму , я даю согласие на "<a href="/privacy-policy">обработку своих персональных данных</a>".',
      '#required' => TRUE,
      '#type' => 'checkbox',


    );



    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Получить инструкцию',
    );

    return $form;
  }
  /**
   * {@inheritdoc}
   * Submit
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $phone = preg_match("([0-9-]+)",$form_state->getValue('phone'),$match);
    $email = preg_match("/[0-9a-z]+@[a-z]/",$form_state->getValue('E-mail'),$match);
    $name = preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/",$form_state->getValue('your_name'),$match);
    $comment =preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/",$form_state->getValue('comment'),$match);
    if(!$phone){
      $form_state->setErrorByName('phone',$this->t('Формат телефона не верен!!!'));

    }

    if(!$email){
      $form_state->setErrorByName('E-mail',$this->t('Формат E-mail не верен!!!'));

    }

    if($name){
      $form_state->setErrorByName('your_name',$this->t('Имя может содержать только русские / латинские символы, пробел, цифры и знак _'));

    }

    if(!empty($form_state->getValue("comment"))){
      if($comment){
        $form_state->setErrorByName('comment',$this->t('Комментарий может содержать только русские / латинские символы, пробел, цифры и знак _'));

      }

    }

    if (empty($form_state->getErrors())){
      \Drupal::messenger()->addMessage($this->t("Форма заполнена Верно =:)"));
    }

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {



  }
}
