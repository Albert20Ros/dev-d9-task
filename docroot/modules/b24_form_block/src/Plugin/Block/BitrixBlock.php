<?php

/**
 * @file
 * Contains \Drupal\b24_form_block\Plugin\Block
 */

namespace Drupal\b24_form_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;

/**
 * Provides a 'b24_form_block' block.
 *
 * @Block(
 *   id = "b24_form_block",
 *   admin_label = @Translation("b24_form_block"),
 *   category = @Translation("Custom")
 * )
 */


class BitrixBlock extends BlockBase{

  public function build() {
    /**
     * {@inheritdoc}
     */
    $form = \Drupal::formBuilder()->getForm('Drupal\b24_form_block\Form\BitrixForm');
    return $form;

  }

}
