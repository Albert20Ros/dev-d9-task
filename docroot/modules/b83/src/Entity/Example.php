<?php

namespace Drupal\b83\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;

/**
* Defines the Example entity.
*
* @ingroup example
*
* @ContentEntityType(
*   id = "example",
*   label = @Translation("Example"),
*   handlers = {
*     "views_data" = "Drupal\views\EntityViewsData",
*   },
*   base_table = "example",
*   entity_keys = {
*     "id" = "id",
*     "uuid" = "uuid",
*   },
* )
*/

class Example extends ContentEntityBase {

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Example entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Example entity.'))
      ->setReadOnly(TRUE);

    // Int field.
    $fields['cart_content_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Example int field'))
      ->setDescription(t('Example int field, a key to cart content record id.'));

    // Record creation date.
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when an example was created.'));

    // String field.
    $fields['order_comment'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Example string field'))
      ->setDescription(t('Example string field, a comment to the order.'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ));

    // Float field.
    $fields['order_total'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Example float field'))
      ->setDescription(t('Example float field, an order total sum.'))
      ->setSettings(array(
        'precision' => 20,
        'scale' => 2,
      ));

    return $fields;
  }

  public function toUrl($rel = 'canonical', array $options = []) {
    // Return default URI as a base scheme as we do not have routes yet.
    return Url::fromUri('base:entity/example/' . $this->id(), $options);
  }
}
