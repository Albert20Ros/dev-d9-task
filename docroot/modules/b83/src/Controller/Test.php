<?php

namespace Drupal\b83\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An test controller.
 */
class Test extends ControllerBase {

  public function content() {
    $sql_count = \Drupal::entityQueryAggregate('example')
    ->aggregate('id', 'count')
    ->execute();

    $total = $sql_count[0]['id_count'];
    $num_per_page = 5;
    $pager = \Drupal::service('pager.manager')->createPager($total, $num_per_page);
    $page = $pager->getCurrentPage();
    $offset = $num_per_page * $page;

    $entity_ids = \Drupal::entityQuery('example')
    ->sort('id', 'ASC')
    ->range($offset, $num_per_page)
    ->execute();

    $rows = [];
    $storage = \Drupal::entityTypeManager()->getStorage('example');

    foreach ($entity_ids as $id) {
      $example = $storage->load($id);

      $rows[] = [
        'id' => $example->id->value,
        'created' => \Drupal::service('date.formatter')->format($example->created->value, 'html_datetime'),
        'order_total' => $example->order_total->value,
      ];
    }

    $header = ['ID', 'Created', 'Order Total'];

    $build = [];

    $build[] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    $build[] = [
      '#type' => 'pager',
    ];
    return $build;
  }
}
